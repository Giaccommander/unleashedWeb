
const gulp = require('gulp');
const plumber = require('gulp-plumber');
const rename = require('gulp-rename');
const browserSync = require('browser-sync');
const gutil = require('gulp-util');
const sourcemaps = require('gulp-sourcemaps');

//SASS -> CSS
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const sassLint = require('gulp-sass-lint');

//HTML
const htmlmin = require('gulp-htmlmin');

//Javascript/TypeScript
const browserify = require('gulp-browserify');
const babel = require('gulp-babel');
const jshint = require('gulp-jshint');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');

// Define Important Variables
const src = './src';
const dest = './dest';
//Reload Browser
const reload = (done) => {
    browserSync.reload();
    done();
};
//localDevServer for browsersyncing
const serve = (done) => {
    browserSync.init({
        server: {
            baseDir: dest
        }        
    });
    done();
}


//Compile sass => css
const css = () => {
    return gulp.src(`${src}/sass/**/*.sass`)
    .pipe(plumber())
    //Lint SASS
    .pipe(sassLint({
        options: {
            formatter:'stylish',
        },
        rules: {
            'no-ids': 1,
            'final-newline': 0,
            'no-mergeable-selectors': 1,
            'indentation': 0
        }
    }))    
    .pipe(sassLint.format())
    //Start Source Map for BrowserDebugging
    .pipe(sourcemaps.init())
    .pipe(sass.sync({
        outputStyle: 'compressed'
    })).on('error',sass.logError)
    //add Sufix
    .pipe(rename({
        basename: 'style',
        suffix: '.min'
    }))
    //Add Autoprefixer & cssNano
    .pipe(postcss([autoprefixer(),cssnano()]))
    .pipe(sourcemaps.write(''))
    .pipe(gulp.dest(`${dest}/css`))
    .pipe(browserSync.stream());
};

//.html => .min
const html = () => {
    return gulp.src(`${src}/*.html`)
        .pipe(plumber())
        .pipe(htmlmin({
            collapseWhitespace:true,
            removeComments: true,
            html5: true,
            removeEmptyAttributes: true,
            removeTagWhitespace: true,
            sortClassName: true

        }))
        .pipe(gulp.dest(`${dest}`));
};

//Complie .js => min.js
const script = () => {
    return gulp.src(`${src}/js/**/*.js`)
    //init Plumber
    .pipe(plumber(((error) => {
        gutil.log(error.message);        
    }))) 
    //Start Source Map for BrowserDebugging
    .pipe(sourcemaps.init())
    //concat
    .pipe(concat('concat.js'))
    //Use Babel
    .pipe(babel())
    //Javascript Lint
    .pipe(jshint())
    //Reporter of jsLint
    .pipe(jshint.reporter('jshint-stylish'))
    //Add Browser support
    .pipe(browserify({
        insertGlobals: true
    }))
    //Minify
    .pipe(uglify())
    //add Sufix    
    .pipe(rename({
        basename: 'global',
        suffix: '.min'
    }))
    //Write Sourcemap for Debugging on Browser
    .pipe(sourcemaps.write(''))      
    //Finaly writeing in the desired Folder
    .pipe(gulp.dest(`${dest}/js`))
    //stream Changes not reload ;)
    .pipe(browserSync.stream());
};


//Project watching
const watch = () => gulp.watch(
    [`${src}/sass/**/*.sass`,
    `${src}/*.html`,
    `${src}/js/**/*.js`], 
gulp.series(script, html, css, reload));

//All Tasks
const dev = gulp.series(script, html, css, serve, watch);

//Project building
const build = gulp.series(script, html, css);

// Default function
exports.dev = dev;
exports.build = build;
exports.default = build;
